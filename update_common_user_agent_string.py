#!/usr/bin/env python3

import re
from logging import *
import urllib.parse
from datetime import datetime

import ahi

# Try to refresh the COMMON_USER_AGENT_STRING about once a week.
useragents_me_client = ahi.HTTPClient(
        # "As the data here don't change sooner than once per week, you shouldn't need to make lots of requests all at once."
        cache_ttl = 60 * 60 * 24 * 7,
        # "Currently, we impose a rate-limit of 15 requests per IP address per hour (even this is probably too many)."
        force_wait_interval = 60 * 60 / 15,
        logging_level = WARNING,
        auto_adjust_for_rate_limiting = True,
        allow_redirects = False,
        # TODO: Eat our own dogfood and report useragents.me API breakage back to ahi package developers.
        #breakage_handler = handle_useragents_me_breakage,
        verify = True,
        )
stats_url = 'https://cdn.jsdelivr.net/gh/microlinkhq/top-user-agents@master/src/index.json'
resp = useragents_me_client.get(stats_url, expect=True)
COMMON_USER_AGENT_STRING = resp.json.pop()

if not(isinstance(COMMON_USER_AGENT_STRING, str) and len(COMMON_USER_AGENT_STRING) > 10):
    raise ValueError(
            f'Expected a User-Agent string. Got {COMMON_USER_AGENT_STRING!r}')

with open('ahi/simple.py', 'r') as f:
    rewritten = re.sub(r"COMMON_USER_AGENT_STRING = '.*", f'COMMON_USER_AGENT_STRING = {COMMON_USER_AGENT_STRING!r} # From {stats_url}, {datetime.now():%Y-%m-%d}.', f.read())
with open('ahi/simple.py', 'w') as f:
    f.write(rewritten)
